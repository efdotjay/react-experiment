
import React from 'react';

export default class ComponentC extends React.Component {
  render() {
    return (
      <div>
        <p>Component <code>C</code></p>
      </div>
    );
  }
}
